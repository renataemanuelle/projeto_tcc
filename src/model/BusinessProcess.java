package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Renata Anhon
 */

@XmlRootElement
@XmlType(propOrder = {"transitions", "places", "arcs"})
public class BusinessProcess {

    private List<Transition> transitions;
    private List<Place> places;
    private List<Arc> arcs;

    //Constructors
    public BusinessProcess() {
        transitions = new ArrayList<>();
        places = new ArrayList<>();
        arcs = new ArrayList<>();
    }

    public BusinessProcess(ArrayList<Transition> transitions, ArrayList<Place> places, ArrayList<Arc> arcs) {
        this.transitions = transitions;
        this.places = places;
        this.arcs = arcs;
    }

    //Getters and Setters
    @XmlElement
    public List<Transition> getTransitions() {
        return transitions;
    }

    public void setTransitions(ArrayList<Transition> transitions) {
        this.transitions = transitions;
    }

    @XmlElement
    public List<Place> getPlaces() {
        return places;
    }

    public void setPlaces(ArrayList<Place> places) {
        this.places = places;
    }

    @XmlElement
    public List<Arc> getArcs() {
        return arcs;
    }

    public void setArcs(ArrayList<Arc> arcs) {
        this.arcs = arcs;
    }
    
    
    public void addTransition(Transition transition) {
        transitions.add(transition);
    }
    
    public void addPlace(Place place) {
        places.add(place);
    }
    
    public void addArc(Arc arc) {
        arcs.add(arc);
    }
    
    @Override
    public String toString() {
        return "BusinessProcess:"
                + "\n-----------------------------------\n"
                + "transitions:\n" + transitions
                + "\n-----------------------------------\n"
                + "places:\n" + places
                + "\n-----------------------------------\n"
                + "arcs:\n" + arcs  + "\n";
    }

}
