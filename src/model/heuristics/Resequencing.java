package model.heuristics;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import model.Transition;

/**
 *
 * @author renata.anhon
 */
public class Resequencing {
    
    private final ArrayList<Transition> transitions;

    public Resequencing(ArrayList<Transition> transitions) {
        this.transitions = transitions;
    }
    
    public ArrayList<Transition> getNoDependenceTransition(){
        /*
        transitionlist is a linked list where each element is a transition with 
        four set attributes previousplaces, nextplaces, input, output
        */
        
        //array of transitions with no outputs used as input in the following transition
        ArrayList<Transition> st = new ArrayList<>();

        boolean intersection_places = false;
        boolean intersection_inputOutput = false;
        
        for (Transition transition1 : transitions) {
            for (Transition transition2 : transitions) {
                for (String pl1 : transition1.getNextPlaces()) {
                    for (String pl2 : transition2.getPreviousPlaces()) {
                        if (pl1.equalsIgnoreCase(pl2)) {
                            intersection_places = true;
                        }
                    }
                }
                if (intersection_places) {
                    for (String str1 : transition1.getOutputs()) {
                        for (String str2 : transition2.getInputs()) {
                            if (str1.equalsIgnoreCase(str2)) {
                                intersection_inputOutput = true;
                            }
                        }
                    } 
                }
                if (intersection_places && !intersection_inputOutput) {
                    st.add(transition1);
                }
            intersection_places = false;
            intersection_inputOutput = false;
            }  
        }
        
        return st;
        /*
        for each transition1 in transitionlist do
            for each transition2 in transitionlist do
                if intersection(transition1.nextplaces, transition2.previousplaces) != Empty
                    and intersection(transition1.outputs, transition2.inputs) == Empty then
                        st.add(transition1)
        return st
        */
    }
    
    public LinkedHashMap<String, Double> getSimilarTransitionsRank(){
        /*
        transitionlist is a list where each element is a transition with an attribute setOfWords
        each element of setOfWords is a word with two attributes: the word and quantity 
        quantity is the number of times the word appears in the transition
        */
        
        //TP: Array, is the list of similar transitions and the percentage of similarity
        LinkedHashMap<String, Double> TP = new LinkedHashMap<>();
        LinkedHashMap<String, Double> tpOrdenado = new LinkedHashMap<>();
        
        int intersection_quantity, union_quantity;
        Transition transition1, transition2;
        
        for(int i = 0; i < transitions.size(); i++) {
            transition1 = transitions.get(i);
            for (int j = i+1; j < transitions.size(); j++) {
                transition2 = transitions.get(j);
                intersection_quantity = 0;
                union_quantity = 0;
                for (String word1 : transition1.getSetOfWords().keySet()) {
                    union_quantity += transition1.getSetOfWords().get(word1);
                    for (String word2 : transition2.getSetOfWords().keySet()) {
                        if(word1.equalsIgnoreCase(word2)) {
                            intersection_quantity += transition1.getSetOfWords().get(word1) + transition2.getSetOfWords().get(word2);
                        }
                    }
                }
                for (String word2 : transition2.getSetOfWords().keySet()) {
                    union_quantity += transition2.getSetOfWords().get(word2);
                }
                if (union_quantity == 0) {
                    TP.put("("+transition1.getId()+" - "+transition1.getName()+", "+transition2.getId()+" - "+transition2.getName()+")", 0.0);
                } else {
                    TP.put("("+transition1.getId()+" - "+transition1.getName()+", "+transition2.getId()+" - "+transition2.getName()+")", (double) (intersection_quantity) / (double) (union_quantity)*100);
                }
            }
        }
        
        while (!TP.isEmpty()) {
            String str = null;
            double valor = -1;
            for (String st : TP.keySet()) {
                if(TP.get(st) > valor) {
                    str = st;
                    valor = TP.get(st);
                }
            }
            tpOrdenado.put(str, valor);
            TP.remove(str);
        }
 
        return tpOrdenado;

    }
    
}
