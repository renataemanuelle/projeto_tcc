package model.heuristics;

import java.util.ArrayList;
import java.util.Collections;
import model.Transition;

/**
 *
 * @author renata.anhon
 */
public class TaskComposition {
    
    private final ArrayList<Transition> transitions;
    
    private ArrayList<Transition> smallTransitions;
    private ArrayList<Transition> largeTransitions;
    
    private double q1, q2, q3, iqr;
    

    public TaskComposition(ArrayList<Transition> transitions) {
        this.transitions = transitions;
    }
    
    /**
     * Sorts transitions by processing time
     * @param transitionsOrderedbyPT 
     */
    private void orderTransitionsbyPT(ArrayList<Transition> transitionsOrderedbyPT) {
        Collections.sort (transitionsOrderedbyPT, (Object o1, Object o2) -> {
            Transition t1 = (Transition) o1;
            Transition t2 = (Transition) o2;
            return t1.getProcessingTime()< t2.getProcessingTime() ? -1 : (t1.getProcessingTime()> t2.getProcessingTime()? +1 : 0);
        });
    }
    
    /**
     * Removes transitions with processing time zero or less than zero
     * @param transitionsOrderedbyPT array with transitions to be removed
     */
    private void removeTransitionsWithPTZeroOrLess (ArrayList<Transition> transitionsOrderedbyPT) {
        int indexAux = -1;
        for (int i = 0; i < transitionsOrderedbyPT.size(); i ++) {
            if (transitionsOrderedbyPT.get(i).getProcessingTime()>0){
                indexAux = i-1;
                i = transitionsOrderedbyPT.size();
            }
        }
        while(indexAux > -1) {
            transitionsOrderedbyPT.remove(indexAux);
            indexAux--;
        }
    }

    /**
     * Based on the application of the box plot and outliers’ theory 
     * to identify small and large tasks considering the tasks processing time.
     * 
     * @return An array[][] where
     *              array[0] corresponds small transitions
     *              array[1] corresponds large transitions
     */   
    public Transition[][] getSmallandLargeTasks() {
        ArrayList<Transition> transitionsOrderedbyPT = new ArrayList<>(transitions);
        orderTransitionsbyPT(transitionsOrderedbyPT);
        removeTransitionsWithPTZeroOrLess(transitionsOrderedbyPT);
        
        int index;
        //lower quartil
        index = (int)((transitionsOrderedbyPT.size()+1)/4);
        if(index > 0) index--;
        q1 = transitionsOrderedbyPT.get(index).getProcessingTime();
                
        //median
        if (transitionsOrderedbyPT.size() % 2 == 0) {
            q2 = (transitionsOrderedbyPT.get((transitionsOrderedbyPT.size()/2)-1).getProcessingTime() + 
                  transitionsOrderedbyPT.get(transitionsOrderedbyPT.size()/2).getProcessingTime()) / 2;
        } else {
            q2 = transitionsOrderedbyPT.get(transitionsOrderedbyPT.size()/2).getProcessingTime();
        }
        
        //upper quartil
        index = (int)(((transitionsOrderedbyPT.size()+1)*3)/4);
        if(index > 0) index--;
        q3 = transitionsOrderedbyPT.get(index).getProcessingTime();
        
        //interquartil range
        iqr = q3 - q1;
        
        smallTransitions = new ArrayList<>();
        largeTransitions = new ArrayList<>();
        
        transitionsOrderedbyPT.stream().forEach((t) -> {
            if (t.getProcessingTime() < (q1 - 1.5*iqr)) {
                smallTransitions.add(t);
            } else if (t.getProcessingTime() > (q3 + 1.5*iqr)) {
                largeTransitions.add(t);
            }
        });

        Transition[][] t = {smallTransitions.toArray(new Transition[smallTransitions.size()]), 
                            largeTransitions.toArray(new Transition[largeTransitions.size()])};
        return t;
 
    }
    
    /**
     * Based on the analyst experience to determine the distance from the median 
     * of processing times to be used as definition to classify tasks as small or large.
     * 
     * @param distance distance percentage from the median
     * @return An array[][] where
     *              array[0] corresponds small transitions
     *              array[1] corresponds large transitions
     */
    public Transition[][] getSmallandLargeTasks(double distance) {
        ArrayList<Transition> transitionsOrderedbyPT = new ArrayList<>(transitions);
        orderTransitionsbyPT(transitionsOrderedbyPT);
        removeTransitionsWithPTZeroOrLess(transitionsOrderedbyPT);
        
        
        //median
        if (transitionsOrderedbyPT.size() % 2 == 0) {
            q2 = (transitionsOrderedbyPT.get((transitionsOrderedbyPT.size()/2)-1).getProcessingTime() + 
                  transitionsOrderedbyPT.get(transitionsOrderedbyPT.size()/2).getProcessingTime()) / 2;
        } else {
            q2 = transitionsOrderedbyPT.get(transitionsOrderedbyPT.size()/2).getProcessingTime();
        }
        
        smallTransitions = new ArrayList<>();
        largeTransitions = new ArrayList<>();
        
        transitionsOrderedbyPT.stream().forEach((t) -> {
            if (t.getProcessingTime() < ((1-distance)*q2)) {
                smallTransitions.add(t);
            } else if (t.getProcessingTime() > (1+distance)*q2) {
                largeTransitions.add(t);
            }
        });

        Transition[][] t = {smallTransitions.toArray(new Transition[smallTransitions.size()]), 
                            largeTransitions.toArray(new Transition[largeTransitions.size()])};
        return t;
 
    }
    
}
