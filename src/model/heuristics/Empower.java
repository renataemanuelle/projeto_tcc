package model.heuristics;

import java.util.ArrayList;
import model.Place;
import model.BusinessProcess;
import model.Transition;

/**
 *
 * @author Renata Anhon
 */
public class Empower {
       
    private final BusinessProcess process;

    public Empower(BusinessProcess process) {
        this.process = process;
    }    
    
    public ArrayList<Transition> getEmpowerTransitions(){
        //placelist is a linked list where each element is a place with two set attributes
        //previousTransitions and nextTransitions
        
        //transitionList is a linked list where each element is a transition with two set
        //attributes previousPlaces and nextPlaces 
        
        //array of places with only 1 previous transition and at least 2 following transition   
        ArrayList<String> ds = new ArrayList<>();
    
        //array of transitions with only 1 previous place and the following place is in ds
        ArrayList<Transition> dt = new ArrayList<>();
    
        //array of transitions to indicate empower redesign pattern application
        ArrayList<Transition> d = new ArrayList<>();
    
        
        for (Place p : process.getPlaces()) {
            if(p.getPreviousTransitions().length==1 && p.getNextTransitions().length >=2) {
                ds.add(p.getId());
            }
        }
        
        for (Transition t : process.getTransitions()) {
            if(t.getPreviousPlaces().length==1) {
                for (String p : t.getNextPlaces()){
                    if (ds.contains(p)) {
                        dt.add(t);
                    }
                }
            }   
        }
        
        for (Transition t1: process.getTransitions()) {
            for (Transition t2: dt) {
                for (String p1 : t1.getNextPlaces()) {
                    for (String p2 : t2.getPreviousPlaces()) {
                        if (t1.getRole() != null && t2.getRole() != null) {
                            if (p1.equals(p2) && !(t1.getRole().equals(t2.getRole())) && !d.contains(t2)) {
                                d.add(t2);
                            }
                        }
                    }
                }
            }
        }
        
        return d;
        /*
        for each place in placelist do
            if length(place.previousTransitions)==1 and  length(place.nextTransitions)>=2 then
                ds.add(place)

        for each transition in transitionlist do
            if length(transition.previousplaces)=1 then
                for each place in transitions.nextplaces do
                    if place in ds then
                        dt.add(transition)

        for each transition1 in transitionlist do 
            for transition2 in dt do
                if intersection (transition1.nextplaces, transition2.previousplaces) != Empty
                    and transition1.role != transition2.role then
                        d.add(transition2)

        return d    
        */

    }
        
}
