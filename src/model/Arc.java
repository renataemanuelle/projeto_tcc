package model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Renata
 */

@XmlRootElement
@XmlType(propOrder = {"source", "target"})
public class Arc {
    
    private String source;
    private String target;

    
    public Arc() {
    }
    
    public Arc(String source, String target) {
        this.source = source;
        this.target = target;
    }

    @XmlElement
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    @XmlElement
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }
    
    
    
}
