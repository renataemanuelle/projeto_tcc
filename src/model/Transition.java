package model;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author Renata Anhon
 */

@XmlRootElement
@XmlType(propOrder = {"id", "name", "description", "processingTime", "inputs", "outputs", "previousPlaces", "nextPlaces", "role", "setOfWords"})
public class Transition {
     private String id;
     private String name;
    
     private String description;

     private int processingTime;
    
     private String[] inputs = new String[0];
     private String[] outputs = new String[0];
    
     private String[] previousPlaces = new String[0];
     private String[] nextPlaces = new String[0];
    
     private String role;
    
     private Map <String, Integer> setOfWords = new LinkedHashMap<>();
        
    //Constructors
    public Transition() {
    }

    public Transition(String id, String name, String description, int processingTime, String[] input, String[] output, String[] previousPlaces, String[] nextPlaces, String role, Map<String, Integer> setOfWords) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.processingTime = processingTime;
        this.inputs = input;
        this.outputs = output;
        this.previousPlaces = previousPlaces;
        this.nextPlaces = nextPlaces;
        this.role = role;
        this.setOfWords = setOfWords;
    }
 
    //Getters and Setters
    @XmlElement 
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    @XmlElement 
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement 
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
   
    @XmlElement 
    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    @XmlElement 
    public String[] getInputs() {
        return inputs;
    }

    public void setInputs(String[] inputs) {
        this.inputs = inputs;
    }

    @XmlElement 
    public String[] getOutputs() {
        return outputs;
    }

    public void setOutputs(String[] outputs) {
        this.outputs = outputs;
    }

    @XmlElement 
    public String[] getPreviousPlaces() {
        return previousPlaces;
    }

    public void setPreviousPlaces(String[] previousPlaces) {
        this.previousPlaces = previousPlaces;
    }

    @XmlElement 
    public String[] getNextPlaces() {
        return nextPlaces;
    }

    public void setNextPlaces(String[] nextPlaces) {
        this.nextPlaces = nextPlaces;
    }

    @XmlElement 
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @XmlElement 
    public Map <String, Integer> getSetOfWords() {
        return setOfWords;
    }

    public void setSetOfWords(Map <String, Integer> setOfWords) {
        this.setOfWords = setOfWords;
    }
    
    public void addNextPlace(String placeId) {
        String[] aux = new String[nextPlaces.length + 1];
        System.arraycopy(nextPlaces, 0, aux, 0, nextPlaces.length);
        aux[aux.length - 1] = placeId;
        nextPlaces = aux;
    }
    
    public void addPreviousPlace(String placeId) {
        String[] aux = new String[previousPlaces.length + 1];
        System.arraycopy(previousPlaces, 0, aux, 0, previousPlaces.length);
        aux[aux.length - 1] = placeId;
        previousPlaces = aux;
    }
    
    public void addInput(String input) {
        String[] aux = new String[inputs.length + 1];
        System.arraycopy(inputs, 0, aux, 0, inputs.length);
        aux[aux.length - 1] = input;
        inputs = aux;
    }
    
    public void addOutput(String output) {
        String[] aux = new String[outputs.length + 1];
        System.arraycopy(outputs, 0, aux, 0, outputs.length);
        aux[aux.length - 1] = output;
        outputs = aux;
    }
    
    public void addWord(String word, int quantity) {
        setOfWords.put(word, quantity);
    }
    
    @Override
    public String toString() {
        return "\nTransition id: " + id + "\n"
                + "name: " + name + "\n"
                + "description: " + description + "\n"
                + "processingTime: " + processingTime + "\n"
                + "inputs: " + Arrays.toString(inputs) + "\n"
                + "outputs: " + Arrays.toString(outputs)  + "\n"
                + "previousPlaces: " + Arrays.toString(previousPlaces) + "\n"
                + "nextPlaces: " + Arrays.toString(nextPlaces) + "\n"
                + "role: " + role + "\n"
                + "setOfWords: " + setOfWords;
    }

}
