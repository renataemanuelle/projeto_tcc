package model;

import java.util.Arrays;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author renata.anhon
 */

@XmlRootElement
@XmlType(propOrder = {"id", "name", "previousTransitions", "nextTransitions"})
public class Place {
 
    private String id;   
    private String name;
    private String[] previousTransitions = new String[0];
    private String[] nextTransitions = new String[0];
    
    //Constructor  
    public Place() {
    }

    public Place(String id, String name, String[] previousTransitions, String[] nextTransitions) {
        this.id = id;
        this.name = name;
        this.previousTransitions = previousTransitions;
        this.nextTransitions = nextTransitions;
    }
    
    public Place(String id, String name) {
        this.id = id;
        this.name = name;
    }
    
    //Getters and Setters
    @XmlElement
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public String[] getPreviousTransitions() {
        return previousTransitions;
    }

    public void setPreviousTransitions(String[] previousTransitions) {
        this.previousTransitions = previousTransitions;
    }

    @XmlElement
    public String[] getNextTransitions() {
        return nextTransitions;
    }

    public void setNextTransitions(String[] nextTransitions) {
        this.nextTransitions = nextTransitions;
    }    
    
    
    public void addNextTransition(String transitionId) {
        String[] aux = new String[nextTransitions.length + 1];
        System.arraycopy(nextTransitions, 0, aux, 0, nextTransitions.length);
        aux[aux.length - 1] = transitionId;
        nextTransitions = aux;
    }
    
    public void addPreviousTransition(String transitionId) {
        String[] aux = new String[previousTransitions.length + 1];
        System.arraycopy(previousTransitions, 0, aux, 0, previousTransitions.length);
        aux[aux.length - 1] = transitionId;
        previousTransitions = aux;
    }
    
    @Override
    public String toString() {
        return "\nPlace id: " + id + "\n"
                + "name: " + name + "\n"
                + "previousTransitions: " + Arrays.toString(previousTransitions) + "\n"
                + "nextTransitions: " + Arrays.toString(nextTransitions);
    }

}
 