package control;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import model.BusinessProcess;

/**
 *
 * @author Renata
 */
public class XMLHandler {
    
    public BusinessProcess readXML(String fileName) {
    
        try {
            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(BusinessProcess.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            BusinessProcess process = (BusinessProcess) jaxbUnmarshaller.unmarshal(file);
            System.out.println(process);
            return process;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public boolean writeXML (String fileName, BusinessProcess process) {
        try {

            File file = new File(fileName);
            JAXBContext jaxbContext = JAXBContext.newInstance(BusinessProcess.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // output pretty printed
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(process, file);
            //jaxbMarshaller.marshal(process, System.out);
            
            return true;

        } catch (JAXBException e) {
                e.printStackTrace();
        }
        return false;
    }
}
