package control;

import model.BusinessProcess;
import view.TelaHeuristicas;
import view.TelaInicial;

/**
 *
 * @author Renata
 */
public class ControleTelas {

    private final XMLHandler tratadorXML = new XMLHandler();
    private TelaHeuristicas telaHeuristicas;
    private TelaInicial telaXML;
    private static BusinessProcess processo = new BusinessProcess();

    public BusinessProcess getProcesso() {
        return processo;
    }
    
    public void lerXML(String nomeArquivo) {
        processo = tratadorXML.readXML(nomeArquivo);
        if (processo != null) {
            telaHeuristicas = new TelaHeuristicas();
            telaHeuristicas.setVisible(true);
        }
        else {
            trocarXML();
            telaXML.XMLInvalido();
        }
    }

    public void trocarXML() {
        telaXML = new TelaInicial();
        telaXML.setVisible(true);
    }

}
