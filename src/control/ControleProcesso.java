package control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import javax.swing.DefaultListModel;
import model.BusinessProcess;
import model.Transition;
import model.heuristics.Empower;
import model.heuristics.Resequencing;
import model.heuristics.TaskComposition;

/**
 *
 * @author Renata
 */
public class ControleProcesso {

    //Listas a serem preenchidas com as devidas transições
    private final DefaultListModel listarTransicoes = new DefaultListModel();
    private final DefaultListModel listarTransicoesFinas = new DefaultListModel();
    private final DefaultListModel listarTransicoesGrossas = new DefaultListModel();
    private final DefaultListModel listarTransicoesFinasAnalista = new DefaultListModel();
    private final DefaultListModel listarTransicoesGrossasAnalista = new DefaultListModel();
    private final DefaultListModel listarTransicoesAumentoDePoder = new DefaultListModel();
    private final DefaultListModel listarTransicoesIndependentes = new DefaultListModel();
    private final DefaultListModel listarTransicoesSimilares = new DefaultListModel();

    //Heuristicas
    private final TaskComposition composicaoDeAtividades;
    private final Empower aumentoDePoder;
    private final Resequencing ressequenciamento;

    private static BusinessProcess processo;

    private final ControleTelas controleTelas = new ControleTelas();

    public ControleProcesso() {
        processo = controleTelas.getProcesso();
        if (!processo.getTransitions().isEmpty() && !processo.getPlaces().isEmpty()) {
            composicaoDeAtividades = new TaskComposition((ArrayList<Transition>) processo.getTransitions());
            aumentoDePoder = new Empower(processo);
            ressequenciamento = new Resequencing((ArrayList<Transition>) processo.getTransitions());
        } else {
            composicaoDeAtividades = null;
            aumentoDePoder = null;
            ressequenciamento = null;
        }
    }

    //Getters 
    public DefaultListModel getListarTransicoes() {
        return listarTransicoes;
    }

    public DefaultListModel getListarTransicoesFinas() {
        return listarTransicoesFinas;
    }

    public DefaultListModel getListarTransicoesGrossas() {
        return listarTransicoesGrossas;
    }

    public DefaultListModel getListarTransicoesFinasAnalista() {
        return listarTransicoesFinasAnalista;
    }

    public DefaultListModel getListarTransicoesGrossasAnalista() {
        return listarTransicoesGrossasAnalista;
    }

    public DefaultListModel getListarTransicoesAumentoDePoder() {
        return listarTransicoesAumentoDePoder;
    }

    public DefaultListModel getListarTransicoesIndependentes() {
        return listarTransicoesIndependentes;
    }

    public DefaultListModel getListarTransicoesSimilares() {
        return listarTransicoesSimilares;
    }

    /**
     * Método para preencher as listas com todas as transições correpondentes
     *
     * @param percentual percentual informado pelo analista
     */
    public void preencheListas(Double percentual) {

        //lista de transições
        if (processo.getTransitions().isEmpty()) {
            listarTransicoes.addElement("Nenhuma transição foi encontrada");
        } else {
            processo.getTransitions().forEach((t) -> {
                listarTransicoes.addElement(t.getId() + " - " + t.getName());
            });

            //lista de composicao de atividades padrão
            Transition[][] composicao = composicaoDeAtividades();
            if (composicao[0].length == 0) {
                listarTransicoesFinas.addElement("Nenhuma transição fina foi encontrada");
            } else {
                for (Transition tf : composicao[0]) {
                    listarTransicoesFinas.addElement(tf.getId() + " - " + tf.getName());
                }
            }
            if (composicao[1].length == 0) {
                listarTransicoesGrossas.addElement("Nenhuma transição grossa foi encontrada");
            } else {
                for (Transition tg : composicao[1]) {
                    listarTransicoesGrossas.addElement(tg.getId() + " - " + tg.getName());
                }
            }

            //lista de composição de atividades pelo analista
            Transition[][] composicaoAnalista = composicaoDeAtividades(percentual);
            if (composicaoAnalista[0].length == 0) {
                listarTransicoesFinasAnalista.addElement("Nenhuma transição fina foi encontrada");
            } else {
                for (Transition tf : composicaoAnalista[0]) {
                    listarTransicoesFinasAnalista.addElement(tf.getId() + " - " + tf.getName());
                }
            }
            if (composicaoAnalista[1].length == 0) {
                listarTransicoesGrossasAnalista.addElement("Nenhuma transição grossa foi encontrada");
            } else {
                for (Transition tg : composicaoAnalista[1]) {
                    listarTransicoesGrossasAnalista.addElement(tg.getId() + " - " + tg.getName());
                }
            }

            //lista de atividades para empoderamento
            if (aumentoDePoder().isEmpty()) {
                listarTransicoesAumentoDePoder.addElement("Nenhuma transição para aumento de poder foi encontrada");
            } else {
                aumentoDePoder().forEach((te) -> {
                    listarTransicoesAumentoDePoder.addElement(te.getId() + " - " + te.getName());
                });
            }
            
            //lista de atividades para resenquenciamento - transicoes independentes
            if (transicoesIndependentes().isEmpty()) {
                listarTransicoesIndependentes.addElement("Nenhuma transição para aumento de poder foi encontrada");
            } else {
                transicoesIndependentes().forEach((te) -> {
                    listarTransicoesIndependentes.addElement(te.getId() + " - " + te.getName());
                });
            }
            
            
            //lista de atividades para resenquenciamento - transicoes similares
            LinkedHashMap <String, Double> transSimi = transicoesSimilares();
            if (transSimi.isEmpty()) {
                listarTransicoesSimilares.addElement("Nenhuma transição para aumento de poder foi encontrada");
            } else { 
                transSimi.keySet().forEach((word) -> {
                    String s = String.format("%.2f - " + word, transSimi.get(word));
                    listarTransicoesSimilares.addElement(s);
                });
            }
            
            
        }
    }

    /**
     * Atualiza detlahes da transição selecionada
     * @param itemSelecionado transição selecionada
     * @return Vetor de string com os atributos da transição onde: [0]: nome
     * [1]: descrição [2]: inputs [3]: outputs [4]: papel [5]: tempo de
     * processamento
     */
    public String[] atualizarCampos(String itemSelecionado) {
        if (!processo.getTransitions().isEmpty()) {
            String[] itemAux = itemSelecionado.split(" - ");
            if (processo.getTransitions() == null) {
                return null;
            }
            Transition t1 = new Transition();
            for (Transition t : processo.getTransitions()) {
                if (t.getId().equals(itemAux[0])) {
                    t1 = t;
                }
            }
            return new String[]{t1.getName(), t1.getDescription(), Arrays.toString(t1.getInputs()), Arrays.toString(t1.getOutputs()), t1.getRole(), String.valueOf(t1.getProcessingTime())};
        }
        return null;
    }

    /**
     * Atualiza lista de Transicoes finas e grossas pela definicao do Analista
     *
     * @param percentual definido pelo analista
     */
    public void atualizarPercentual(Double percentual) {
        if (!processo.getTransitions().isEmpty()) {
            listarTransicoesFinasAnalista.clear();
            listarTransicoesGrossasAnalista.clear();
            Transition[][] composicao = composicaoDeAtividades(percentual);
            for (Transition tf : composicao[0]) {
                listarTransicoesFinasAnalista.addElement(tf.getId() + " - " + tf.getName());
            }
            for (Transition tg : composicao[1]) {
                listarTransicoesGrossasAnalista.addElement(tg.getId() + " - " + tg.getName());
            }
        }
    }

    //Chamadas aos metodos das classes de Heuristicas
    private Transition[][] composicaoDeAtividades() {
        return composicaoDeAtividades.getSmallandLargeTasks();
    }

    private Transition[][] composicaoDeAtividades(Double percentual) {
        return composicaoDeAtividades.getSmallandLargeTasks(percentual / 100);
    }

    private ArrayList<Transition> aumentoDePoder() {
        return aumentoDePoder.getEmpowerTransitions();
    }

    private ArrayList<Transition> transicoesIndependentes() {
        return ressequenciamento.getNoDependenceTransition();
    }

    private LinkedHashMap<String, Double> transicoesSimilares() {
        return ressequenciamento.getSimilarTransitionsRank();
    }
}
